package ru.kuzin.tm;

import ru.kuzin.tm.constant.ArgumentConst;
import ru.kuzin.tm.constant.CommandConst;
import ru.kuzin.tm.util.TerminalUtil;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
        processCommand();
    }

    private static void processCommand() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("\nENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showErrorCommand() {
        System.err.println("[ERROR]");
        System.err.println("Current command is not correct!");
        System.exit(1);
    }

    private static void showErrorArgument() {
        System.err.println("[ERROR]");
        System.err.println("Current argument is not correct!");
    }

    private static void processCommand(final String argument) {
        switch (argument) {
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                showErrorCommand();
        }
    }

    private static void processArgument(final String argument) {
        switch (argument) {
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showErrorArgument();
        }
    }

    private static void showVersion() {
        System.out.println("\n[VERSION]");
        System.out.println("1.6.0");
    }

    private static void showAbout() {
        System.out.println("\n[ABOUT]");
        System.out.println("Name: Kuzin Igor");
        System.out.println("E-mail: garbir9@mail.ru");
    }

    private static void showHelp() {
        System.out.println("\n[HELP]");
        System.out.printf("%s, %s - Show version info.\n", CommandConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s - Show developer info.\n", CommandConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s - Show command list.\n", CommandConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s - Close application.\n", CommandConst.EXIT);
    }

}